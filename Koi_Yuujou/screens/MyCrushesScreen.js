import React from 'react';
import { StyleSheet, View, Text, ScrollView} from 'react-native';
import { Avatar, Icon} from 'react-native-elements';
import Header from '../components/HeaderForBack';
import Crush from '../components/Crush';


export default class MyCrushesScreen extends React.Component {
    renderCrush(){
      const crush = [];
      const data = [
        {
          name: "Will",
          platform: "Facebook",
          age: 22,
          status: "Single",
          about: "Being nice is always not my \nbusiness"
        },
        {
          name: "Eric",
          platform: "Facebook",
          age: 22,
          status: "Single",
          about: "Being nice is always not my \nbusiness"
        },
        {
          name: "Hunter",
          platform: "Facebook",
          age: 22,
          status: "Single",
          about: "Being nice is always not my \nbusiness"
        },
        {
          name: "Kelvin",
          platform: "Facebook",
          age: 22,
          status: "Single",
          about: "Being nice is always not my \nbusiness"
        },
        {
          name: "Eric02",
          platform: "Facebook",
          age: 22,
          status: "Single",
          about: "Being nice is always not my \nbusiness"
        }
      ]
      const rank = [1,2,3,4,5]
      const color = ["#f8b2cc", "#7cb3db", "#ffb75f", "#91e9de", "#f8dc7e"];
      for(let i = 0; i < 5; i++){
        crush.push(
        <View key={i} style={{backgroundColor:color[i]}}>
          <Crush ranking={rank[i]}
            name={data[i].name}
            platform={"Friend on " + data[i].platform}
            age={data[i].age + " years old"}
            status={data[i].status}
            about={data[i].about}
          />
        </View>
        );
      }
      return crush;
    }
    render() {
      return (
        <View style={{flexDirection: 'column', flex:1}}>
          <View>
            <Header centerText="My Crushes"/>
          </View>
          <ScrollView style={{flexDirection: 'column', flex:1}}>
          {this.renderCrush()}
          </ScrollView>
        </View>
      );
    }
  }
  
  
  const styles = StyleSheet.create({
    description:{
      fontSize:15,
      marginBottom:5,
    }
  });