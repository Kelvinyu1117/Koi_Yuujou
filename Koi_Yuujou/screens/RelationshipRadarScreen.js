import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import { Avatar, Header, Icon} from 'react-native-elements';
import Slider from "react-native-slider";

export default class RelationshipRadarScreen extends React.Component {
 
    render() {
      return (
      <View>
        <Header
          backgroundColor="white"
          centerComponent={
            <Text style={styles.headerTitleFont}>
              Relationship Radar
            </Text>
          }
          rightComponent={{icon: 'clear',  color: '#e1e4e9'}}/>
        <View>
          <View style={{flexDirection: 'row', marginTop: 25, marginLeft: 25}}>
            <Text style={{color: '#47525e', fontSize: 14, fontWeight:'bold'}}>Your EXPECTED ratings:</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
          <View style={{flexDirection: 'column', marginRight: 10}}>
                <Text style={styles.SliderCaption}>Buddies</Text>
                <Text style={styles.SliderCaption}>Work</Text>
                <Text style={styles.SliderCaption}>Family</Text>
                <Text style={styles.SliderCaption}>Schoolmates</Text>
                <Text style={styles.SliderCaption}>Lovers</Text> 
                <Text style={styles.SliderCaption}>Mentors</Text>
          </View>
          <View style={{flexDirection: 'column'}}>
            <View>
              <Slider
                style={{width:210, marginTop: 18}}
                trackStyle={sliderInnerStyles.track}
                thumbStyle={sliderInnerStyles.thumb}
                maximumValue={5}
                step={1}
                minimumTrackTintColor='#8795a9'
                maximumTrackTintColor='#edeff6'
              />
              <Slider
                style={{width:210, marginTop: 8}}
                trackStyle={sliderInnerStyles.track}
                thumbStyle={sliderInnerStyles.thumb}
                maximumValue={5}
                step={1}
                minimumTrackTintColor='#8795a9'
                maximumTrackTintColor='#edeff6'
              /> 
              <Slider
                style={{width:210, marginTop: 5}}
                trackStyle={sliderInnerStyles.track}
                thumbStyle={sliderInnerStyles.thumb}
                maximumValue={5}
                step={1}
                minimumTrackTintColor='#8795a9'
                maximumTrackTintColor='#edeff6'
              /> 
              <Slider
                style={{width:210, marginTop: 5}}
                trackStyle={sliderInnerStyles.track}
                thumbStyle={sliderInnerStyles.thumb}
                maximumValue={5}
                step={1}
                minimumTrackTintColor='#8795a9'
                maximumTrackTintColor='#edeff6'
              /> 
              <Slider
                style={{width:210, marginTop: 8}}
                trackStyle={sliderInnerStyles.track}
                thumbStyle={sliderInnerStyles.thumb}
                maximumValue={5}
                step={1}
                minimumTrackTintColor='#8795a9'
                maximumTrackTintColor='#edeff6'
              /> 
              <Slider
                style={{width:210, marginTop: 5}}
                trackStyle={sliderInnerStyles.track}
                thumbStyle={sliderInnerStyles.thumb}
                maximumValue={5}
                step={1}
                minimumTrackTintColor='#8795a9'
                maximumTrackTintColor='#edeff6'
              />  
            </View>
          </View>
          </View>
        </View>
      </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    headerTitleFont :{
      color: '#47525e',
      fontSize: 20,
      fontWeight: 'bold',
    },
    headerIcon:{
      color: '#e1e4e9',
    },
    SliderCaption:{
      color: '#47525e', 
      fontSize: 14, 
      marginTop: 30, 
      marginLeft: 20,
    }
  });
  
  const sliderInnerStyles = StyleSheet.create({
    track: {
      height: 4,
      borderRadius: 2,
    },
    thumb: {
      width: 20,
      height: 20,
      borderRadius: 20 / 2,
      backgroundColor: 'white',
      borderColor: '#8795a9',
      borderWidth: 2,
    }
  });