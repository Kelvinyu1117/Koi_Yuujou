import React from 'react';
import { StyleSheet, View, Image, Alert } from 'react-native';
import { Button, Text, Icon } from 'react-native-elements';
import { RkButton } from 'react-native-ui-kitten';
import { Facebook } from 'expo';


export default class LoginScreen extends React.Component {
  fbToken;

  constructor(props) {
    super(props);
    this._logIn = this._logIn.bind(this);
    this.test1 = this.test1.bind(this);
  }

  _logIn = async () => {
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('1219236874875448', {
      permissions: ['public_profile', 'user_friends', 'public_profile'],
    });
    this.fbToken = token;
    console.log('logined=' + this.fbToken);
    if (type === 'success') {
      // Get the user's name using Facebook's Graph API
      /*response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);

      response.json().then(json => {
        debugger
        Alert.alert(json);
      });*/
      /*
      Alert.alert(
        'Logged in!',
        `Hi ${(await response.json()).name}!`
      );
      */
      /*
      response = await fetch(`https://graph.facebook.com/me/friends?access_token=${this.fbToken}`);
      response.json().then(json => {
        debugger
        //Alert.alert(json);
      });
      */
    }
  }

  async test1() {
    console.log('fbToken=' + this.fbToken);
    response = await fetch(`https://graph.facebook.com/v1.0/me/friends?access_token=${this.fbToken}`);
    response.json().then(json => {
      console.log(json);
      //Alert.alert(json);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../src/img/fb-icon.png')}
          style={styles.fbIcon} />
        <View style={styles.innerContainer}>
          <RkButton
            style={styles.fbLoginBtn}
            contentStyle={styles.btnTitle}
            onPress={this._logIn}>
            Login with Facebook
          </RkButton>
          <Button
            onPress={this.test1}
            title="Test 1"
          />
        </View>
        <View style={styles.innerContainer}>
          <Text
            style={{ color: '#9da4ad', textAlign: 'center', fontSize: 18, marginBottom: 10 }}>
            Koi will receive your public{'\n'}profile.
          </Text>
          <View style={{ flexDirection: 'row', marginBottom: 16 }}>
            <Icon
              name="info-outline"
              size={16}
              color='#68caff'
            />
            <Text
              style={{ marginLeft: 5, color: '#68caff', textAlign: 'center', fontSize: 16 }}>
              Learn More
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Icon
              name="lock"
              size={12}
              color='#9da4ad'
            />
            <Text
              style={{ marginLeft: 5, color: '#9da4ad', textAlign: 'center', fontSize: 12 }}>
              This doesn't let the app post to Facebook
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  fbIcon: {
    width: 100,
    height: 100,
  },
  fbLoginBtn: {
    backgroundColor: '#47525e',
    borderWidth: 0,
    borderRadius: 5,
    height: 55,
    width: 200,
    marginTop: 20,
    marginBottom: 20,
  },
  btnTitle: {
    color: 'white',
    fontWeight: "400",
    fontSize: 20,
  }
});


