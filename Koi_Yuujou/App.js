import React from 'react';
import { StyleSheet, View, Text, ScrollView} from 'react-native';
import { Avatar, Icon} from 'react-native-elements';
import Header from './components/HeaderForBack';
import Crush from './components/Crush';
import LoginScreen from './screens/LoginScreen';
//import RelationshipRadarScreen from './screens/RelationshipRadarScreen';
import MyCrushesScreen from './screens/MyCrushesScreen';

export default class App extends React.Component {
  render() {
    return (
      <LoginScreen />
    );
  }
}

const styles = StyleSheet.create({
  description:{
    fontSize:15,
    marginBottom:5,
  }
});
