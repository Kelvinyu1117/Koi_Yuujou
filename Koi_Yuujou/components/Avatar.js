import { Avatar } from 'react-native-elements';
import { ImagePicker } from 'expo';

export default class Avatar extends React.Component {
    state = {
      image: null,
    };
  
    _pickImage = async () =>{
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });
      if (!result.cancelled) {
        this.setState({ image: result.uri });
      }
    }
    
    render() {
      let { image } = this.state;
      return (
        <Avatar 
            large
            rounded
            source={{uri: image}}
            onPress={this._pickImage}
            style={{marginTop:30}}
        />
      );
    }
  }
  