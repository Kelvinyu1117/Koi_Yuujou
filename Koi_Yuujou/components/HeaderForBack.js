import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import { Avatar, Header, Icon} from 'react-native-elements';

export default class HeaderForBack extends React.Component {
 
    render() {
      return (
        <Header
          backgroundColor="white"
          leftComponent={{icon: 'keyboard-arrow-left',  color: '#e1e4e9', size: 25}}
          centerComponent={
            <Text style={styles.headerTitleFont}>
              {this.props.centerText}
            </Text>
          }
        />
      );
    }
  }
  
  const styles = StyleSheet.create({
    headerTitleFont :{
      color: '#47525e',
      fontSize: 20,
      fontWeight: 'bold',
    },
  });
  