import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import { Avatar, Icon} from 'react-native-elements';
import Header from './HeaderForBack';


export default class Crush extends React.Component {
 
    render() {
      return (
            <View style={{flexDirection:'row', flex:1, marginTop:15}}>
            <View style={{marginRight:120}}>
                <Text style={{fontSize:30}}>{this.props.ranking}</Text>
            </View>
            <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginRight: 25}}>
                <View style={{marginBottom:10}}>
                <Avatar 
                    large
                    rounded
                    style={{marginTop:30}}
                    source={{uri: "https://i.imgur.com/kKWf6Im.jpg"}}
                />
                </View>
                <View style={{ marginBottom:10}}>
                    <Text style={styles.name}>{this.props.name}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'column', marginRight: 5}}>
                <Text style={styles.description}>{this.props.platform}</Text>
                <Text style={styles.description}>{this.props.age}</Text>
                <Text style={styles.description}>{this.props.status}</Text>
                <Text style={styles.description}>{this.props.about}</Text>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    name:{
        fontSize:15
    },
    description:{
        fontSize:12,
        marginBottom:10,
    }
});